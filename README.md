Newsletter Generator
====================

Ein kleines Tool um HTML Seiten zu erzeugen, die als
Newsletter versendet werden können.

Vorteile aus meiner Sicht:

* Bilder werden eingebettet (keine externen Server, Keine Datenschutzbedenken)
* Layout sollte auf vielen Bildschirmgrößen und mit vielen E-Mail-Programmen funktionieren


Bekannte Nachteile

* Die Links im Inhaltsverzeichnis funktionieren nicht überall
  z.B. gmail/gmx-android-app


Notwendige Software
-------------------

* Windows (7 und 10 getestet) und Linux (Linuxmint 19.1) sind getestet
* pandoc (**mind. Version 2.1.3**)(Installiert und im Systempfad Verfügbar)  
  Kann heruntergeladen werden unter:
  <https://github.com/jgm/pandoc/releases/download/2.1.3/pandoc-2.1.3-windows.msi>
* Schriftarten installiert 
* Optional für Pdf-Erzeugung `wkhtmltopdf` installiert und **im Systempfad verfügbar**
  (muss im cmd script einkommentiert werden)


Benutzung
---------

* Am besten immer das ganze Verzeichnis kopieren und von einer
  alten Version ausgehen!
* Bilder im Unterverzeichnis `bilder` hinzufügen/entfernen wie nötig
    * Da können beliebig viele Bilder drin liegen. In der HTML-Seiten
      landen nur die tatsächlich verwendeten
    * Darauf achten, dass sie mindestens 900 Pixel
      breit sind  
      Im Beispiel hier sind die Bilder alle im Format
      10:2 zugeschnitten (900x180 Pixel),  
      sie können aber auch höher sein.
    * stark komprimieren (vorzugsweise jpg, da
      dieses Format sehr kleine Dateien zulässt)
      Ein Bild sollte nicht größer als 10kB bis 15kB sein um auch
      mobil und bei schlechter Verbindung schnell runtergeladen zu sein.
    * Insgesamt sollte die Ergebnis HTML nicht größer als
      2MB werden um keine Probleme bei Empfänger*innen zu
      verursachen, die nur relativ kleine Mails empfangen
      zu können.
* Die Texte liegen in den Dateien im Unterverzeichnis
  `inhalte` Sie können mit jedem reinen Texteditor (also z.B.
  Notepad, Editor, Notepad++, Atom, ... ) geöffnet/bearbeitet
  werden (Nicht geeignet sind Textverarbeitungsprogramme
  wie Word oder Writer)
* Zum erzeugen der HTML-Seite muss unter Windows nur auf
  `newsletter_generator.cmd` doppelt geklickt werden.
  Wenn alles richtig installiert ist erscheint im
  Unterverzeichnis `ergebnis` eine Datei die als Newsletter
  verschickt werden kann.  
  Unter Linux doppelklick und "Im Terminal ausführen" wählen!

Wie schreibe ich
----------------

Inhalt und Hauptteil werden in der Textauszeichungssprache Markdown geschrieben. Siehe <https://de.wikipedia.org/wiki/Markdown>

Ende und Impressum werden in html bearbeitet.



Versand
-------

Klappt z.B. mit Thunderbird und dem Add-on `ThunderHTMLedit`
mit dessen Hilfe sich HTML-Code leicht in eine neue
Nachricht kopieren lässt.

1. ergebnis/Newsletter.html mit texteditor öffnen und alles kopieren
2. In Thunderbird eine neue E-Mail `Verfassen`
3. Mit dem `ThunderHTMLedit` Add-on gibt es dort den Reiter HTML. Dort
   erst alles löschen und dann den Text aus dem Texteditor einfügen!
4. Zurück in den Reiter `Editieren` wechseln und prüfen ob alles richtig aussieht!
5. Adressieren und versenden

-----------------------------------------------------
