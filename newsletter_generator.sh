#!/bin/bash

BUILD_DIR_NAME="build_greenNLG"
WORK_DIR=$(pwd)

DEBUG=0
if [[ $1 == "-d" ]]; then
    DEBUG=1
    shift
fi

INPUT=$1

echo "greenNLG"

# ----------------------------------------------
# script base directory
if [ -n "${BASH_SOURCE}" ]; then
    SCRIPT="${BASH_SOURCE[0]}"
else
    SCRIPT="$0"
fi
[ -h "$SCRIPT" ] && SCRIPT=$(readlink "$SCRIPT")

BASE_DIR=$(cd -P -- "$(dirname -- "$SCRIPT")" && pwd -P)


if [[ $DEBUG = 1 ]]; then
    echo "Base Directory: $BASE_DIR"
    echo "Work Directory: $WORK_DIR"
fi

# clean (delete) build directory if already there
rm -rf ${BUILD_DIR_NAME}

# create $BUILD_DIR_NAME sub directory
mkdir "$BUILD_DIR_NAME"

# enter $BUILD_DIR_NAME sub directory
cd "$BUILD_DIR_NAME" || exit








# ----------------------------------------------
# HTML self contained for E-Mail
# ----------------------------------------------

cp "$BASE_DIR/vorlagen/anschreiben_html.html" "$WORK_DIR/$BUILD_DIR_NAME"
cp "$BASE_DIR/vorlagen/hauptteil_html.html" "$WORK_DIR/$BUILD_DIR_NAME"

# Anschreiben first 
pandoc --template="$WORK_DIR/$BUILD_DIR_NAME/anschreiben_html.html" -M lang=de -o "$WORK_DIR/$BUILD_DIR_NAME/gen_anschreiben.html" "$WORK_DIR/inhalte/anschreiben.md" "$WORK_DIR/inhalte/metadaten.yaml"
# main document
pandoc --template="$WORK_DIR/$BUILD_DIR_NAME/hauptteil_html.html" -M lang=de --standalone --self-contained -B "$WORK_DIR/$BUILD_DIR_NAME/gen_anschreiben.html" -A "$WORK_DIR/inhalte/ende_und_impressum.html" --toc -o "$WORK_DIR/ergebnis/Newsletter.html" "$WORK_DIR/inhalte/hauptteil.md" "$WORK_DIR/inhalte/metadaten.yaml"

# If there is no LaTex wkhtm is an option
# pdf creation from mail html by wkhtmltopdf
# wkhtmltopdf -s A4 -B 6.5 -L 6.5 -R 6.5 -T 6.5 "$WORK_DIR/ergebnis/Newsletter.html" "$WORK_DIR/ergebnis/Newsletter_wk.pdf"


# ----------------------------------------------
# LaTeX
# ----------------------------------------------

#copy document class and templates
ln -s "$BASE_DIR/vorlagen/"* .

#link figures (if the subdir exists)
if [[ -d "$WORK_DIR/bilder" ]]; then
    ln -s "$WORK_DIR/bilder" .
fi

# ----------------------------------------------
# convert md -> tex -> pdf
# Anschreiben first 
pandoc --template="$WORK_DIR/$BUILD_DIR_NAME/anschreiben_pdfpandoc.tex" -o _generated_anschreiben.tex "$WORK_DIR/inhalte/anschreiben.md" "$WORK_DIR/inhalte/metadaten.yaml"
# main document
pandoc --template="$WORK_DIR/$BUILD_DIR_NAME/hauptteil_pdfpandoc.tex" -M lang=de --standalone -B ./_generated_anschreiben.tex -A "$WORK_DIR/inhalte/ende_und_impressum.tex" --toc -o _generated.tex "$WORK_DIR/inhalte/hauptteil.md" "$WORK_DIR/inhalte/metadaten.yaml"

# run lualatex
lualatex -interaction nonstopmode -file-line-error _generated.tex
# second pass for the table of contents
lualatex -interaction nonstopmode -file-line-error _generated.tex

# ----------------------------------------------
# move pdf
if [[ -e "_generated.pdf" ]]; then
    cp _generated.pdf "$WORK_DIR/ergebnis/newsletter.pdf"
fi

#cd "$WORK_DIR" || exit
#if [[ $DEBUG -eq 0 ]]; then
#    rm -rf "$BUILD_DIR_NAME"
#fi
