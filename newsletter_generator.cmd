@ECHO OFF

REM ----------------------------------------------
REM set build dir name
set build_dir_name=intermediate_build_dir

REM ----------------------------------------------
REM work directory
set workdir=%cd%

REM ----------------------------------------------
REM script base directory
set base=%~dp0
cd  /d %base%
set base=%cd%

cd /d %workdir%

REM ----------------------------------------------
REM clean (delete) build directory if already there
if exist "%workdir%\%build_dir_name%" rmdir /s /q "%workdir%\%build_dir_name%"||GOTO END

REM ----------------------------------------------
REM create %build_dir_name% sub directories 
mkdir "%workdir%\%build_dir_name%"

REM ----------------------------------------------
REM enter %build_dir_name% sub directory
cd "%workdir%\%build_dir_name%"

REM ----------------------------------------------
REM LaTeX
REM ----------------------------------------------

REM ----------------------------------------------
REM copy document class and templates
copy "%base%\vorlagen\*" "%workdir%\%build_dir_name%"

REM copy figures
mkdir "%workdir%\%build_dir_name%\figures"
xcopy /s /y "%workdir%\bilder" "%workdir%\%build_dir_name%\figures"

REM ----------------------------------------------
REM convert md -> tex -> pdf
REM Anschreiben first 
pandoc --template="%workdir%\%build_dir_name%\anschreiben_pdfpandoc.tex" -o _generated_anschreiben.tex "%workdir%\inhalte\anschreiben.md" "%workdir%\inhalte\metadaten.yaml"
REM main document
pandoc --template="%workdir%\%build_dir_name%\hauptteil_pdfpandoc.tex" -M lang=de --standalone -B .\_generated_anschreiben.tex -A "%workdir%\inhalte\ende_und_impressum.tex" --toc -o _generated.tex "%workdir%\inhalte\hauptteil.md" "%workdir%\inhalte\metadaten.yaml"

REM run lualatex
lualatex -interaction nonstopmode -file-line-error _generated.tex
REM second pass for the table of contents
lualatex -interaction nonstopmode -file-line-error _generated.tex

REM ----------------------------------------------
REM move pdf
if exist _generated.pdf copy _generated.pdf "%workdir%\ergebnis\newsletter.pdf"

REM ----------------------------------------------
REM HTML self contained for E-Mail
REM ----------------------------------------------
REM Anschreiben first 
pandoc --template="%workdir%\%build_dir_name%\anschreiben_html.html" -M lang=de -o "%workdir%\%build_dir_name%\gen_anschreiben.html" "%workdir%\inhalte\anschreiben.md" "%workdir%\inhalte\metadaten.yaml"
REM main document
pandoc --template="%workdir%\%build_dir_name%\hauptteil_html.html" -M lang=de --standalone --self-contained -B "%workdir%\%build_dir_name%\gen_anschreiben.html" -A "%workdir%\inhalte\ende_und_impressum.html" --toc -o "%workdir%\ergebnis\Newsletter.html" "%workdir%\inhalte\hauptteil.md" "%workdir%\inhalte\metadaten.yaml"

REM If there is no LaTeX you can use wkhtmltopdf
REM pdf creation from mail html by wkhtmltopdf
REM wkhtmltopdf -s A4 -B 6.5 -L 6.5 -R 6.5 -T 6.5 "%workdir%\ergebnis\Newsletter.html" "%workdir%\ergebnis\Newsletter_wk.pdf"

goto end 

:END
REM ----------------------------------------------
REM back to start
cd /d %workdir%
if exist "%workdir%\%build_dir_name%" rmdir /s /q "%workdir%\%build_dir_name%"