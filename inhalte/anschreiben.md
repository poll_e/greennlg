Sehr geehrte Damen und Herren, liebe Freundinnen und Freunde,

**Hier steht eigentlich das Anschreiben des Newsletters**

Ich habe ein kleines Tool und eine Vorlage geschrieben 
um HTML Seiten zu erzeugen, die als Newsletter 
versendet werden können.

Vorteile aus meiner Sicht:

* Trennung von Inhalt und Aussehensbeschreibung (Die Texte werden einfach in Textdateien hinterlegt
  das Tool kümmert sich um das Layout und die Bereitstellung der HMTL-Seite bzw. einer pdf)
* Versand klappt mit jedem E-Mail-System/Programm in dem man HTML/Multipart-Mail direkt reinkopieren kann z.B. Thunderbird mit dem ThunderHTMLedit Add-on.
* Basis für die erzeugung sind die Free and Open Source Software/Programme (FOSS) 
    * pandoc (erzeugung der HTML - Seite)
    * LaTeX (pdf)
* Bilder werden eingebettet (keine externen Server, Keine Datenschutzbedenken) Im Beispiel hier sind die Bilder alle im Format 10:2 zugeschnitten (900x180 Pixel), sie können aber auch höher sein. 
* Layout sollte jetztauf mehr Bildschirmgrößen und in mehr Mailprogrammen funktionieren 

Der nächste Schritt währe, die Redakteur*innen auf dem Tool zu schulen, alles bei Ihnen einzurichten und zu klären wo es noch Verbesserungsbedarf gibt und ob und mit welchem Zeithorizont der umsetzbar ist.

Das Tool und die Vorlagen sind als zip Datei angehangen. Etwas mehr Hilfe steht in 
der README.md darin drin. Alles noch in einem frühen Stadium, aber vermutlich gibt es 
eh eine andere professionellere Lösung, so dass ich hier erst einmal nicht den Doku
Aufwand übertreiben möchte;-)

Viele Grüße

Matthias

<hr>

**Doku:**

**Benutzung**

* Am besten immer das ganze Verzeichnis kopieren und von einer
  alten Version ausgehen!
* Bilder im Unterverzeichnis `bilder` hinzufügen/entfernen wie nötig
    * Da können beliebig viele Bilder drin liegen. In der HTML-Seiten
      landen nur die tatsächlich verwendeten
    * Darauf achten, dass sie mindestens 900 Pixel
      breit sind  
      Im Beispiel hier sind die Bilder alle im Format
      10:2 zugeschnitten (900x180 Pixel),  
      sie können aber auch höher sein.
    * stark komprimieren (vorzugsweise jpg, da
      dieses Format sehr kleine Dateien zulässt)
      Ein Bild sollte nicht größer als 10kB bis 15kB sein um auch
      mobil und bei schlechter Verbindung schnell runtergeladen zu sein.
    * Insgesamt sollte die Ergebnis HTML nicht größer als
      2MB werden um keine Probleme bei Empfänger*innen zu
      verursachen, die nur relativ kleine Mails empfangen
      zu können.
* Die Texte liegen in den Dateien im Unterverzeichnis
  `inhalte` Sie können mit jedem reinen Texteditor (also z.B.
  Notepad, Editor, Notepad++, Atom, ... ) geöffnet/bearbeitet
  werden (Nicht geeignet sind Textverarbeitungsprogramme
  wie Word oder Writer)
* Zum erzeugen der HTML-Seite muss nur auf
  `newsletter_generator.cmd` doppelt geklickt werden.
  Wenn alles richtig installiert ist erscheint im
  Unterverzeichnis `ergebnis` eine Datei die als Newsletter
  verschickt werden kann.


**Versand**

Klappt z.B. mit Thunderbird und dem Add-on `ThunderHTMLedit`
mit dessen Hilfe sich HTML-Code leicht in eine neue
Nachricht kopieren lässt.

1. ergebnis/Newsletter.html mit texteditor öffnen und alles kopieren
2. In Thunderbird eine neue E-Mail `Verfassen`
3. Mit dem `ThunderHTMLedit` Add-on gibt es dort den Reiter HTML. Dort
   erst alles löschen und dann den Text aus dem Texteditor einfügen!
4. Zurück in den Reiter `Editieren` wechseln und prüfen ob alles richtig aussieht!
5. Adressieren und versenden

